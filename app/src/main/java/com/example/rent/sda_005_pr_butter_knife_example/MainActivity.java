package com.example.rent.sda_005_pr_butter_knife_example;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    private static final String LINE_1_CONTENT = "SDA jest fajne.";
    private static final String LINE_2_CONTENT = "SDA jest jeszcze fajniejsze.";

    @BindView(R.id.text_view_one)
    protected TextView textViewOne;

    @BindView(R.id.text_view_two)
    protected TextView textViewTwo;

    @BindView(R.id.button_one)
    protected Button buttonOne;

    @BindView(R.id.button_two)
    protected Button buttonTwo;

    @BindView(R.id.button_three)
    protected Button buttonThree;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.button_one, R.id.button_two, R.id.button_three})
    protected void onButtonsCLick(View v) {

        if (v instanceof Button) {
            Button vCastedToButton = (Button) v;

            if (vCastedToButton == buttonOne) {
                textViewOne.setText(LINE_1_CONTENT);
            } else if (vCastedToButton == buttonTwo) {
                textViewTwo.setText(LINE_2_CONTENT);
            } else {
                textViewOne.setText("");
                textViewTwo.setText("");
            }
        }
    }
}
